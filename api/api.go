package api

import (
	"fmt"

	appContext "ht-log-demo/context"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func getStackTrace(err error) string {
	type stackTracer interface {
		StackTrace() errors.StackTrace
	}

	e, ok := err.(stackTracer)
	if !ok {
		return ""
	}

	// для этого типа ошибки свои форматеры
	return fmt.Sprintf("%+v", e)
}

// Availability - our example function where we show logging
// requestId - извне либо генерим
func Availability(ctx context.Context, requestId string, param1 string, param2 string) {

	// вытаскиваем из контекста логер, у которого уже указано поле applicationInstanceId (см. main.go)
	logger := appContext.GetLogger(ctx)

	// подменяем новый логер, который будет добавлять дефолтные поля к сообщению
	// специфичные для Availability
	logger = logger.WithFields(log.Fields{
		"requestId": requestId,
		// помимо кастомных полей logrus всегда добавляет
		// - time. The timestamp when the entry was created.
		// - level. The logging level.
	})

	// создаем контекст для Availability
	ctx = appContext.WithLogger(ctx, logger)

	// дальше у нас везде новый логер из Availability
	logger.Info("Availability start")
	defer logger.Info("Availability stop")

	logger.Debug("Execute step1")
	step1(ctx)

	logger.Debug("Execute step2")
	step2(ctx)

	logger.Debug("Execute stepWithError")
	if err := stepWithError(ctx); err != nil {

		// we are at top level, so log error now
		logger.WithFields(log.Fields{
			"stackTrace": getStackTrace(err),
			// добавляем в лог какие-то важные поля чтобы был понятен контекст вызова
			"param1": param1,
			"param2": param2,
		}).WithError(err).Error("Availability error on step N")
	}
}

// our logic for Availability func bellow

// logic step
func step1(ctx context.Context) {
	logger := appContext.GetLogger(ctx)
	logger.Info("step1 start")
	defer logger.Info("step1 stop")
	// some logic
	// ...

	// all go well
}

// logic step
func step2(ctx context.Context) {
	logger := appContext.GetLogger(ctx)
	logger.Info("step2 start")
	defer logger.Info("step2 stop")
	// some logic
	// ...

	// all go well
}

func stepWithError(ctx context.Context) error {
	logger := appContext.GetLogger(ctx)
	logger.Info("stepWithError start")
	defer logger.Info("stepWithError stop")

	err := nestedStep1(ctx)
	if err != nil {
		// do not log error, propagate on top level
		return err
	}

	// some logic
	// ...

	return nil
}

func nestedStep1(ctx context.Context) error {
	logger := appContext.GetLogger(ctx)
	logger.Info("nestedStep1 start")
	defer logger.Info("nestedStep1 stop")

	err := nestedStep2(ctx)
	if err != nil {
		// do not log error, propagate on top level
		return err
	}

	// some logic
	// ...

	return nil
}

func nestedStep2(ctx context.Context) error {
	logger := appContext.GetLogger(ctx)
	logger.Info("nestedStep2 start")
	defer logger.Info("nestedStep2 stop")

	err := sourceOfError(ctx)
	if err != nil {
		// do not log error, propagate on top level
		return err
	}

	// some logic
	// ...

	return nil
}

// какая-то логика, где мы определили, что у нас ошибка,
// либо вызов функции из внешней библиотеки, которая вернула ошибку
func sourceOfError(ctx context.Context) error {
	logger := appContext.GetLogger(ctx)
	logger.Info("sourceOfError start")
	defer logger.Info("sourceOfError stop")

	// эмулируем, что вызов внешнего метода вернул ошибку
	err := fmt.Errorf("some huge error")

	if err != nil {
		// врапим оригинальную ошибку в ошибку со стектрейсом
		// errors пакадж - это не системный, а https://github.com/pkg/errors
		// не путать с https://golang.org/pkg/errors/
		return errors.Wrap(err, "external api call")
	}

	// some logic
	// ...

	return nil
}
