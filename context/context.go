package context

import (
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// честно стырено с docker/swarmkit

type (
	loggerKey struct{}
	moduleKey struct{}
)

// WithLogger returns a new context with the provided logger. Use in
// combination with logger.WithField(s) for great effect.
func WithLogger(ctx context.Context, logger *log.Entry) context.Context {
	return context.WithValue(ctx, loggerKey{}, logger)
}

// GetLogger retrieves the current logger from the context. If no logger is
// available, the default logger is returned.
func GetLogger(ctx context.Context) *log.Entry {
	logger := ctx.Value(loggerKey{})

	if logger == nil {
		return log.NewEntry(log.StandardLogger())
	}

	return logger.(*log.Entry)
}
