package main

import (
	"ht-log-demo/api"
	appContext "ht-log-demo/context"

	"golang.org/x/net/context"

	graylogHook "github.com/gemnasium/logrus-graylog-hook"
	log "github.com/sirupsen/logrus"
)

func main() {
	instanceId := "генерим application instance id"
	// передаем настройки graylog из нашего конфига
	hook := graylogHook.NewAsyncGraylogHook("<graylog_ip>:<graylog_port>", map[string]interface{}{"this": "is logged every time"})
	defer hook.Flush()
	log.AddHook(hook)

	// меняем уровень логирования в зависимости от переменной среды LOG_LEVEL
	// в зависимости от этого меняется наш лог
	log.SetLevel(log.DebugLevel)

	logger := log.WithFields(log.Fields{
		"applicationInstanceId": instanceId,
	})

	ctx := appContext.WithLogger(context.TODO(), logger)
	api.Availability(ctx, "request101", "a", "b")
}
